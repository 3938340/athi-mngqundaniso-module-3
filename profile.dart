import 'package:flutter/material.dart';
import 'package:four_developer/Screens/dashboard.dart';


void main(){
  runApp(const MyApp());
}
class MyApp extends StatelessWidget{
const MyApp({Key? key}): super(key: key);

@override
Widget build(BuildContext context){
  return MaterialApp(
      title: 'DBSS',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: Scaffold(
        appBar: AppBar( title: const Text("Edit Profile"),
            leading: IconButton(
              onPressed: () { },
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.black,
              ),
            ),
              actions: [
                IconButton(
                    icon: const Icon(
                    Icons.settings,
                    color: Colors.grey,
                    ),
                    onPressed: (){}
      )
              ],
            ),
        body: ProfileA(),
      )
  );
}
}
class ProfileA extends StatefulWidget{
  @override
  _ProfileAState createState() => _ProfileAState();
}

class _ProfileAState extends State<ProfileA>{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 14.0),
          children:  <Widget>[
            SizedBox(height:50.0,),
            TextField(
              decoration: InputDecoration(
                labelText: "Email",
                labelStyle: TextStyle(fontSize:15),
                filled: true,
              ),
            ),
            SizedBox(height:15.0,),
            TextField(
              obscureText: true,
              decoration: InputDecoration(
                labelText: "Password",
                labelStyle: TextStyle(fontSize:15),
                filled: true,
               ),
            ),
            SizedBox(height:15),
            Column(
                children: <Widget>[
                  MaterialButton(
                    height: 50,
                    disabledColor: Colors.grey,
                    disabledElevation: 4.0,
                    onPressed: () {Navigator.push(context, MaterialPageRoute(
                        builder: (context)=>DashboardA()
                    ));  },
                    child: ElevatedButton(
                      onPressed: null,
                      child: Text('Save',style:TextStyle(fontSize:15,color: Colors.black)),
                    ),
                  ),
                ],
            ),
      ]
    ),
    ),
    );
  }

}
