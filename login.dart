import 'package:flutter/material.dart';
import 'package:four_developer/Screens/register.dart';

import 'dashboard.dart';


void main(){
  runApp(const MyApp());
}
class MyApp extends StatelessWidget{
  const MyApp({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'DBSS',
      home: LoginA(),
    );
  }
}
class LoginA extends StatefulWidget{
  @override
  _LoginAState createState() => _LoginAState();
}
class _LoginAState extends State<LoginA>{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SafeArea(
        child:ListView(
          padding: const EdgeInsets.symmetric(horizontal: 14.0),
          children: <Widget>[
            Column(
              children: <Widget>[
                const SizedBox(height: 150,),
                Image.asset('assets/sicon.png'),
                const SizedBox(height: 25,),
                const Text('DBSS Login',style: TextStyle(fontSize: 15, color: Colors.orangeAccent),)
            ],
           ),
            const SizedBox(height:50.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "Username/Email",
                labelStyle: TextStyle(fontSize:15),
                filled: true,
              ),
            ),
            const SizedBox(height:15.0,),
            const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    labelText: "Password",
                    labelStyle: TextStyle(fontSize:15),
                    filled: true,
                ),
            ),
            const SizedBox(height:15),
            Column(
              children: <Widget>[
                MaterialButton(
                  height: 50,
                  disabledColor: Colors.grey,
                  disabledElevation: 4.0,
                  onPressed: () { Navigator.push(context, MaterialPageRoute(
                      builder: (context)=>DashboardA()
                  )); },
                  child:const ElevatedButton(
                    onPressed: null,
                    child: Text('Login',style:TextStyle(fontSize:15,color: Colors.black)),
                  ),
                ),
                const SizedBox(height:15,),
                Column(
                    children: <Widget>[
                      MaterialButton(
                        height: 20,
                        disabledColor: Colors.grey,
                        disabledElevation: 4.0,
                        onPressed: () { Navigator.push(context, MaterialPageRoute(
                            builder: (context)=>RegisterA()
                        ));  },
                        child:const ElevatedButton(
                          onPressed: null,
                          child: Text('Register',style:TextStyle(fontSize:15,color: Colors.black)),
                        ),
                      ),
                      const SizedBox(height:15,),
                      const Text('Forgot Password'),
              ]
            )
          ]
        )
      ]
    )
    )
    );
  }
}